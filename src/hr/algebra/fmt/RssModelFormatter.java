/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.fmt;

import hr.algebra.model.Director;
import hr.algebra.model.Actor;
import hr.algebra.model.Genre;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;

/**
 *
 * @author inetjojo
 */
public class RssModelFormatter {

    private static String[] splitData(String data) {
        return data.split(",");
    }

    public static List<Director> parseDirector(String data) {
        List<Director> directors = new ArrayList<>();
        String[] items = splitData(data);
        for (String item : items) {
            directors.add(new Director(item));
        }

        return directors;
    }
    
    public static String parseDescription(String descriptionRaw){
        return Jsoup.parse(descriptionRaw).text();
    }

    public static List<Actor> parseActor(String actorsRaw) {
        List<Actor> actors = new ArrayList<>();
        if (actorsRaw == null || actorsRaw.isEmpty()) {
            return actors;
        }
        
        String[] items = splitData(actorsRaw);
        for (String item : items) {
            actors.add(new Actor(item));
        }

        return actors;
    }
    
     public static List<Genre> parseGenre(String actorsRaw) {
        List<Genre> genres = new ArrayList<>();
        if (actorsRaw == null || actorsRaw.isEmpty()) {
            return genres;
        }
        
        String[] items = splitData(actorsRaw);
        for (String item : items) {
            genres.add(new Genre(item));
        }

        return genres;
    }

    public static String modelsToLabelFormat(List<ViewModel> viewModels) {
        String[] directorsRaw = viewModels.stream().map(it -> it.getName()).toArray(String[]::new);
        return String.join(",", directorsRaw);
    }
    
}
