/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.hr.cinestar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 *
 * @author inetjojo
 */
public class RssCacheManager {
    private final File cacheDir = new File("cache/images");

    //generates local application cache directory
    private void generateRssCacheStorage() {
        cacheDir.mkdirs();
    }

    public RssCacheManager() {
        generateRssCacheStorage();
    }
    
    private String generateFilePath(String filename){
        return String.format("%s/%s.jpg", cacheDir.getAbsolutePath(), filename);
    }
    
    public boolean isCached(String filename){
        File file = new File(generateFilePath(filename));
        return file.exists();
    }

    public boolean processFileCache(byte[] data, String filename) throws Exception {
        try (FileOutputStream stream = new FileOutputStream(generateFilePath(filename))) {
            stream.write(data);
        }catch(Exception e){
            throw e;
        }
        
        return false;
    }
    
    public byte[] getCachedFile(String filename) throws IOException{
        File file = new File(generateFilePath(filename));
        return Files.readAllBytes(file.toPath());
    }

}
