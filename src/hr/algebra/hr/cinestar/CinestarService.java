/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.hr.cinestar;

import hr.algebra.model.Item;
import hr.algebra.model.RssFeed;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 *
 * @author inetjojo
 */
public interface CinestarService {
  @GET("rss.aspx")
  Call<RssFeed> getNajavaByID(@Query("najava") int najavaID);
  
  @GET
  Call<ResponseBody> getPlakat(@Url String url);
}
