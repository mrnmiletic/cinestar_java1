/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.hr.cinestar;

import hr.algebra.model.Item;
import hr.algebra.model.Movie;
import hr.algebra.model.RssFeed;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import hr.algebra.api.CinestarAPI;
import hr.algebra.model.Actor;
import hr.algebra.model.Director;
import hr.algebra.model.Genre;

/**
 *
 * @author inetjojo
 */
public class RssAPI implements CinestarAPI {

    private final String BASE_URL = "https://www.blitz-cinestar.hr/";

    private Retrofit.Builder builder = new Retrofit.Builder().
            baseUrl(BASE_URL).
            addConverterFactory(SimpleXmlConverterFactory.create());

    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private CinestarService rssClient;

    private RssCacheManager cache = new RssCacheManager();

    private List<Movie> movies = new ArrayList<>();

    //initaties http connection to remote service
    private void initateServiceConnections() {
        Retrofit retrofit = builder.build();
        rssClient = retrofit.create(CinestarService.class);
    }

    public RssAPI() throws IOException {
        initateServiceConnections();
        fillMoviesCache();
    }

    private void fillMoviesCache() throws IOException {
        Call<RssFeed> callSync = rssClient.getNajavaByID(1);

        Response<RssFeed> response = callSync.execute();
        if (response.isSuccessful()) {
            RssFeed feed = response.body();
            List<Item> items = feed.channel.items;
            items.forEach(item -> {
                if (!cache.isCached(item.getPosterName())) {
                    Response<ResponseBody> plakatResponse;
                    try {
                        plakatResponse = rssClient.getPlakat(item.posterURL).execute();
                        if (plakatResponse.isSuccessful()) {
                            byte[] imageData = plakatResponse.body().bytes();
                            cache.processFileCache(imageData, item.getPosterName());
                            item.image = imageData;
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(RssAPI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        item.image = cache.getCachedFile(item.getPosterName());
                    } catch (IOException ex) {
                        Logger.getLogger(RssAPI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                movies.add(item.toMovie());
            });
        }
    }

    @Override
    public List<Movie> getMovies() throws Exception {
        return movies;
    }

    @Override
    public Movie getMovieByID(int id) throws Exception {
        Optional<Movie> op = movies.stream().filter(it -> it.id == id).findFirst();
        if (!op.isPresent()) {
            throw new Exception("movie with that id does not exist");
        }

        return op.get();
    }

    @Override
    public Movie updateMovie(Movie movie) throws Exception {
        Optional<Movie> mo = movies.stream().filter(it -> it.id == movie.id).findFirst();
        if (!mo.isPresent()) {
            throw new Exception("movie with that id does not exist");
        }
        Movie existingMovie = mo.get();

        existingMovie.description = movie.description;
        existingMovie.title = movie.title;
        existingMovie.lenght = movie.lenght;

        return existingMovie;
    }

    @Override
    public void addMovie(Movie movie) throws Exception {
        movies.add(movie);
    }

    @Override
    public void deleteMovie(Movie movie) throws Exception {
        movies.remove(movie);
    }
    
    @Override
    public void addActorToMovie(int movieID, Actor actor) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().actors.add(actor);
    }

    
    @Override
    public void removeActorFromMovie(int movieID, Actor actor) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().actors.remove(actor);
    }

    @Override
    public void addDirectorToMovie(int movieID, Director director) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().directors.add(director);
    }

    @Override
    public void addGenreToMovie(int movieID, Genre genre) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().genres.add(genre);
    }

    @Override
    public void removeDirectorFromMovie(int movieID, Director director) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().directors.remove(director);
    }

    @Override
    public void removeGenreFromMovie(int movieID, Genre genre) throws Exception {
        Optional<Movie> om = movies.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().genres.remove(genre);
    }
}
