/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.autenticationManager;

import hr.algebra.db.CinestarDB;
import hr.algebra.db.SQLConnSingleton;
import hr.algebra.model.Director;
import hr.algebra.model.User;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import javax.sql.DataSource;

/**
 *
 * @author inetjojo
 */
public class AuthenticationManager {

    private static User currentUser;
    private static final String AUTH_USER = "{ CALL authUser (?,?) }";

    public static boolean authUser(User user) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(AUTH_USER)) {
            stmt.setString(1, user.username);
            stmt.setString(2, user.password);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    currentUser = User.fromResultSet(rs);
                    return true;
                }
            }
        }
        return false;
    }

    public static User getCurrentUser() {
        return currentUser;
    }
}
