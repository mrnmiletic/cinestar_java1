/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.api;

import hr.algebra.model.Actor;
import hr.algebra.model.Director;
import hr.algebra.model.Genre;
import hr.algebra.model.Movie;
import java.util.List;

/**
 *
 * @author inetjojo
 */

public interface CinestarAPI {
    List<Movie> getMovies() throws Exception;
    
    Movie getMovieByID(int id) throws Exception;
    Movie updateMovie(Movie movie) throws Exception;
    void addMovie(Movie movie) throws Exception;
    void deleteMovie(Movie movie) throws Exception;
    void addActorToMovie(int movieID, Actor actor) throws Exception;
    void addDirectorToMovie(int movieID, Director director) throws Exception;
    void addGenreToMovie(int movieID, Genre genre) throws Exception;
    void removeActorFromMovie(int movieID,Actor actor) throws Exception;
    void removeDirectorFromMovie(int movieID,Director director) throws Exception;
    void removeGenreFromMovie(int movieID,Genre genre) throws Exception;
}
