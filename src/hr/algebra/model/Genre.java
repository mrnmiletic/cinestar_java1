/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import hr.algebra.fmt.ViewModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author inetjojo
 */
public class Genre implements ViewModel{
    private static int idCounter = 0;
    private static final String ID = "ID";
    private static final String NAME = "Name";

    public int id;
    public String name;
    
    public Genre(String name, int id) {
        this.id = id;
        this.name = name;
    }

    public Genre(String name) {
        idCounter++;
        this.id = idCounter;
        this.name = name;
    }

    public static Genre fromResultSet(ResultSet rs) throws SQLException {
        return new Genre(rs.getString(NAME), rs.getInt(ID));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
