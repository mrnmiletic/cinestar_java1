/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author inetjojo
 */
public class User {
    private static final String USERNAME = "Username";
    private static final String PASSWORD = "Password";
    private static final String IS_ADMIN = "isAdmin";
    
    public String username;
    public String password;
    private boolean isAdmin;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public User(String username, String password,boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
    }
    
    
    public static User fromResultSet(ResultSet rs) throws SQLException {
        return new User(rs.getString(USERNAME), rs.getString(PASSWORD), rs.getBoolean(IS_ADMIN));
    }
    
    public boolean isAdmin(){
        return isAdmin;
    }
    
}
