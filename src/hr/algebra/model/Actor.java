/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import hr.algebra.fmt.ViewModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author inetjojo
 */
public class Actor implements ViewModel{
    private static int idCounter = 0;
    private static final String ID = "ID";
    private static final String NAME = "Name";
    
    public String name;
    public int id;
    
    public Actor(String name, int id){
        this.name = name;
        this.id = id;
    }

    public Actor(String item) {
        idCounter++;
        name = item;
        id = idCounter;
    }
    
    public static Actor fromResultSet(ResultSet rs) throws SQLException {
        return new Actor(rs.getString(NAME), rs.getInt(ID));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
