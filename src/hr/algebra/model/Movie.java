/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import hr.algebra.fmt.ViewModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;

/**
 *
 * @author inetjojo
 */
public class Movie implements ViewModel{
    
    private static final String ID = "ID";
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Description";
    private static final String LENGTH = "Length";
    private static final String POSTER_URL = "PosterURl";
    
    public int id;
    public ImageIcon imageIcon;
    public String title;
    public String description;
    public List<Director> directors;
    public List<Actor> actors;
    public String posterURL;
    public List<Genre> genres;
    public int lenght;
    
    public String getPosterName(){
        int begin = posterURL.lastIndexOf("/");
        int end = posterURL.lastIndexOf(".");
        
        return posterURL.subSequence(begin, end).toString();
    }

    public static Movie fromResultSet(ResultSet rs) throws SQLException {
        Movie movie = new Movie();
        movie.id = rs.getInt(ID);
        movie.title = rs.getString(TITLE);
        movie.description = rs.getString(DESCRIPTION);
        movie.lenght = rs.getInt(LENGTH);
        movie.posterURL = rs.getString(POSTER_URL);
        return movie;
    }

    @Override
    public String getName() {
        return title;
    }
}
