/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author inetjojo
 */

@Root(name = "rss", strict = false)
public class RssFeed 
{
    @Element
    public RssChannel channel;
     
    @Override
    public String toString() {
        return "RssFeed [channel=" + channel + "]";
    }
}
