/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 *z
 * @author inetjojo
 */

@Root(name = "rss", strict = false)
public class RssChannel {
    
    @Element
    private String title;
    
    @Element
    private String link;
     
    @Element
    private String description;
     
    @ElementList(inline = true, required = false, name="movie")
    public List<Item> items;
 
    @Override
    public String toString() {
        return "Channel [image=" + title + ", item=" + link + "]";
    }
}
