/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.model;

import hr.algebra.fmt.RssModelFormatter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author inetjojo
 */
@Root(name = "item", strict = false)
public class Item {

    private static int idCounter = 0;

    public byte[] image;

    @Element(name = "title")
    private String title;

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "description")
    private String description;

    @Element(name = "orignaziv")
    private String origNaziv;

    @Element(name = "redatelj")
    private String directorRaw;

    @Element(name = "glumci", required = false)
    private String glumci;

    @Element(name = "trajanje")
    private String trajanje;

    @Element(name = "zanr")
    private String zanr;

    @Element(name = "plakat")
    public String posterURL;

    public String getPosterName() {
        int begin = posterURL.lastIndexOf("/");
        int end = posterURL.lastIndexOf(".");

        return posterURL.subSequence(begin, end).toString();
    }

//    
//    @Element(name = "link")
//    private String link;
//    
//    @Element(name = "guid")
//    private String guid;
//    
//    @Element(name = "slike")
//    private String slike;
//    
//    @Element(name = "trailer")
//    private String trailer;
//    
    @Element(name = "pocetak")
    private String pocetak;

    public Movie toMovie() {
        Movie movie = new Movie();
        movie.id = idCounter++;
        movie.title = title;
        movie.description = RssModelFormatter.parseDescription(description);
        movie.directors = RssModelFormatter.parseDirector(directorRaw);
        movie.imageIcon = new ImageIcon(image);
        movie.actors = RssModelFormatter.parseActor(glumci);
        movie.genres = RssModelFormatter.parseGenre(zanr);
        
        movie.posterURL = posterURL;
        movie.lenght = Integer.parseInt(trajanje);
        return movie;
    }
}
