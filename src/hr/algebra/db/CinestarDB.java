/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.db;

import hr.algebra.model.Movie;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import hr.algebra.api.CinestarAPI;
import hr.algebra.hr.cinestar.RssCacheManager;
import hr.algebra.model.Actor;
import hr.algebra.model.Director;
import hr.algebra.model.Genre;
import java.io.IOException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author inetjojo
 */
public class CinestarDB implements CinestarAPI {

    private RssCacheManager cache = new RssCacheManager();
    private List<Movie> movieCache = new ArrayList<>();

    private static final String GET_ALL_MOVIES = "{ CALL getAllMovies }";
    private static final String GET_MOVIE_BY_ID = "{ CALL getMovieByID (?) }";
    private static final String UPDATE_MOVIE = "{ CALL updateMovie (?,?,?,?) }";
    private static final String DELETE_MOVIE = "{ CALL deleteMovie (?) }";
    private static final String ADD_MOVIE = "{ CALL addMovie (?,?,?,?,?) }";
    private static final String ADD_ACTOR_TO_MOVIE = "{ CALL addActorToMovie (?,?,?) }";
    private static final String REMOVE_ACTOR_FROM_MOVIE = "{ CALL removeActorFromMovie (?,?) }";
    private static final String GET_MOVIE_ACTORS = "{ CALL getMovieActors (?) }";
    private static final String ADD_DIRECTOR_TO_MOVIE = "{ CALL addDirectorToMovie (?,?,?) }";
    private static final String REMOVE_DIRECTOR_FROM_MOVIE = "{ CALL removeDirectorFromMovie (?,?) }";
    private static final String GET_MOVIE_DIRECTORS = "{ CALL getMovieDirectors (?) }";
    private static final String ADD_GENRE_TO_MOVIE = "{ CALL addGenreToMovie (?,?,?) }";
    private static final String REMOVE_GENRE_FROM_MOVIE = "{ CALL removeGenreFromMovie (?,?) }";
    private static final String GET_MOVIE_GENRES = "{ CALL getMovieGenres (?) }";

    private int execAddGenreToMovie(int movieID, String name) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(ADD_GENRE_TO_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setString(2, name);
            stmt.registerOutParameter(3, Types.INTEGER);
            stmt.executeUpdate();
            return stmt.getInt("insertedID");
        }
    }

    private List<Genre> execGetMovieGenres(int movieID) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        List<Genre> genres = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(GET_MOVIE_GENRES)) {
            stmt.setInt(1, movieID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    genres.add(Genre.fromResultSet(rs));
                }
            }
        }
        return genres;
    }

    private int execAddDirectorToMovie(int movieID, String name) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(ADD_DIRECTOR_TO_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setString(2, name);
            stmt.registerOutParameter(3, Types.INTEGER);
            stmt.executeUpdate();
            return stmt.getInt("insertedID");
        }
    }

    private List<Director> execGetMovieDirectors(int movieID) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        List<Director> directors = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(GET_MOVIE_DIRECTORS)) {
            stmt.setInt(1, movieID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    directors.add(Director.fromResultSet(rs));
                }
            }
        }
        return directors;
    }

    private int execAddActorToMovie(int movieID, String name) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(ADD_ACTOR_TO_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setString(2, name);
            stmt.registerOutParameter(3, Types.INTEGER);
            stmt.executeUpdate();
            return stmt.getInt("insertedID");
        }
    }

    @Override
    public void addActorToMovie(int movieID, Actor actor) throws Exception {
        int insertedID = execAddActorToMovie(movieID, actor.name);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        actor.id = insertedID;
        om.get().actors.add(actor);
    }

    private List<Actor> execGetMovieActors(int movieID) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        List<Actor> actors = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(GET_MOVIE_ACTORS)) {
            stmt.setInt(1, movieID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    actors.add(Actor.fromResultSet(rs));
                }

            }
        }
        return actors;
    }

    private List<Movie> execGetAllMovies() throws SQLException {
        List<Movie> movies = new ArrayList<Movie>();
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(GET_ALL_MOVIES);
                ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                Movie movie = Movie.fromResultSet(rs);
                try {
                    movie.imageIcon = new ImageIcon(cache.getCachedFile(movie.getPosterName()));

                } catch (IOException ex) {
                    Logger.getLogger(CinestarDB.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

                movie.actors = execGetMovieActors(movie.id);
                movie.directors = execGetMovieDirectors(movie.id);
                movie.genres = execGetMovieGenres(movie.id);
                movies.add(movie);
            }
        }

        return movies;
    }

    @Override
    public List<Movie> getMovies() throws Exception {
        movieCache = execGetAllMovies();
        return movieCache;
    }

    private Movie execGetMovieByID(int id) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(GET_MOVIE_BY_ID)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    Movie movie = Movie.fromResultSet(rs);
                    try {
                        movie.imageIcon = new ImageIcon(cache.getCachedFile(movie.getPosterName()));

                    } catch (IOException ex) {
                        Logger.getLogger(CinestarDB.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return null;
        }
    }

    @Override
    public Movie getMovieByID(int id) throws Exception {
        return execGetMovieByID(id);
    }

    private Movie execUpdateMovie(Movie movie) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(UPDATE_MOVIE)) {
            stmt.setInt(1, movie.id);
            stmt.setString(2, movie.description);
            stmt.setInt(3, movie.lenght);
            stmt.setString(4, movie.title);
            stmt.executeUpdate();
            return movie;
        }
    }

    @Override
    public Movie updateMovie(Movie movie) throws Exception {
        return execUpdateMovie(movie);
    }

    private void execDeleteMovie(Movie movie) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(DELETE_MOVIE)) {
            stmt.setInt(1, movie.id);
            stmt.executeUpdate();
        }
    }

    @Override
    public void deleteMovie(Movie movie) throws Exception {
        execDeleteMovie(movie);
        movieCache.remove(movie);
    }

    private int execAddMovie(Movie movie) throws SQLException {
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(ADD_MOVIE)) {
            stmt.setString(1, movie.description);
            stmt.setInt(2, movie.lenght);
            stmt.setString(3, movie.title);
            stmt.setString(4, movie.posterURL);
            stmt.registerOutParameter(5, Types.INTEGER);
            stmt.executeUpdate();

            return stmt.getInt("id");
        }
    }

    @Override
    public void addMovie(Movie movie) throws Exception {
        int movieID = execAddMovie(movie);
        movie.actors.forEach(actor -> {
            try {
                execAddActorToMovie(movieID, actor.name);
            } catch (SQLException ex) {
                Logger.getLogger(CinestarDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        movie.directors.forEach(director -> {
            try {
                execAddDirectorToMovie(movieID, director.name);
            } catch (SQLException ex) {
                Logger.getLogger(CinestarDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        movie.genres.forEach(genre -> {
            try {
                execAddGenreToMovie(movieID, genre.name);
            } catch (SQLException ex) {
                Logger.getLogger(CinestarDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    private void execRemoveActorFromMovie(int movieID, int actorID) throws SQLException{
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(REMOVE_ACTOR_FROM_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setInt(2, actorID);
            stmt.executeUpdate();
        }
    }
    
    private void execRemoveDirectorFromMovie(int movieID, int directorID) throws SQLException{
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(REMOVE_DIRECTOR_FROM_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setInt(2, directorID);
            stmt.executeUpdate();
        }
    }
    
    private void execRemoveGenreFromMovie(int movieID, int genreID) throws SQLException{
        DataSource dataSource = SQLConnSingleton.getInstance();
        try (Connection con = dataSource.getConnection();
                CallableStatement stmt = con.prepareCall(REMOVE_GENRE_FROM_MOVIE)) {
            stmt.setInt(1, movieID);
            stmt.setInt(2, genreID);
            stmt.executeUpdate();
        }
    }

    @Override
    public void removeActorFromMovie(int movieID, Actor actor) throws Exception {
        execRemoveActorFromMovie(movieID, actor.id);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().actors.remove(actor);
    }

    @Override
    public void addDirectorToMovie(int movieID, Director director) throws Exception {
        int insertedID = execAddDirectorToMovie(movieID, director.name);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        director.id = insertedID;
        om.get().directors.add(director);
    }

    @Override
    public void addGenreToMovie(int movieID, Genre genre) throws Exception {
        int insertedID = execAddGenreToMovie(movieID, genre.name);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        genre.id = insertedID;
        om.get().genres.add(genre);
    }

    @Override
    public void removeDirectorFromMovie(int movieID, Director director) throws Exception {
        execRemoveDirectorFromMovie(movieID, director.id);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().directors.remove(director);
    }

    @Override
    public void removeGenreFromMovie(int movieID, Genre genre) throws Exception {
        execRemoveGenreFromMovie(movieID, genre.id);
        Optional<Movie> om = movieCache.stream().filter(it -> it.id == movieID).findFirst();
        if (!om.isPresent()) {
            throw new Exception("Movie not found");
        }
        om.get().genres.remove(genre);
    }
}
