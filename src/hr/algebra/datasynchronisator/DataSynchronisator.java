/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.datasynchronisator;

import hr.algebra.db.CinestarDB;
import hr.algebra.hr.cinestar.RssAPI;
import hr.algebra.model.Movie;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author inetjojo
 */
public class DataSynchronisator {
       private static RssAPI remoteAPI;
       private static CinestarDB localDB;
       
       
       static {
           try {
               init();
           } catch (IOException ex) {
               Logger.getLogger(DataSynchronisator.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       
       private static void init() throws IOException{
           remoteAPI = new RssAPI();
           localDB = new CinestarDB();
       }
       
       public static void processDataSyncronisation() throws Exception{
           List<Movie> remoteData = remoteAPI.getMovies();
           remoteData.forEach(movie ->{
               try {
                   localDB.addMovie(movie);
               } catch (Exception ex) {
                   Logger.getLogger(DataSynchronisator.class.getName()).log(Level.SEVERE, null, ex);
               }
           });
           
       }
       
}
