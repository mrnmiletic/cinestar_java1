USE [master]
GO
/****** Object:  Database [cinestar_db]    Script Date: 6/18/2020 12:05:41 AM ******/
CREATE DATABASE [cinestar_db]

USE [cinestar_db]
go
CREATE TABLE [dbo].[Actor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actor_Movie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actor_Movie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActorID] [int] NULL,
	[MovieID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppUser]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUser](
	[Username] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[isAdmin] [bit] NOT NULL,
UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Director]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Director](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Director_Movie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Director_Movie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DirectorID] [int] NULL,
	[MovieID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genre_Movie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genre_Movie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GenreID] [int] NULL,
	[MovieID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [text] NULL,
	[Length] [int] NULL,
	[PosterURl] [nvarchar](255) NULL,
 CONSTRAINT [PK__Movie__3214EC278AD896FA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actor_Movie]  WITH CHECK ADD FOREIGN KEY([ActorID])
REFERENCES [dbo].[Actor] ([ID])
GO
ALTER TABLE [dbo].[Actor_Movie]  WITH CHECK ADD  CONSTRAINT [FK__Actor_Mov__Movie__29572725] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([ID])
GO
ALTER TABLE [dbo].[Actor_Movie] CHECK CONSTRAINT [FK__Actor_Mov__Movie__29572725]
GO
ALTER TABLE [dbo].[Director_Movie]  WITH CHECK ADD FOREIGN KEY([DirectorID])
REFERENCES [dbo].[Director] ([ID])
GO
ALTER TABLE [dbo].[Director_Movie]  WITH CHECK ADD  CONSTRAINT [FK__Director___Movie__2F10007B] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([ID])
GO
ALTER TABLE [dbo].[Director_Movie] CHECK CONSTRAINT [FK__Director___Movie__2F10007B]
GO
ALTER TABLE [dbo].[Genre_Movie]  WITH CHECK ADD FOREIGN KEY([GenreID])
REFERENCES [dbo].[Genre] ([ID])
GO
ALTER TABLE [dbo].[Genre_Movie]  WITH CHECK ADD  CONSTRAINT [FK__Genre_Mov__Movie__34C8D9D1] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([ID])
GO
ALTER TABLE [dbo].[Genre_Movie] CHECK CONSTRAINT [FK__Genre_Mov__Movie__34C8D9D1]
GO
/****** Object:  StoredProcedure [dbo].[addActorToMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addActorToMovie]
@movieID int,
@actorName nvarchar(255),
@insertedID int OUTPUT
as
declare @gID int;
if not exists (select * from Actor where Name = @actorName)
	begin
		insert into Actor(Name) values (@actorName);
		set @gID = SCOPE_IDENTITY();
	end
else
	begin
		select top 1 @gID = id from Actor where Name = @actorName;
	end
insert into Actor_Movie(ActorID,MovieID) values(@gID, @movieID);
set @insertedID = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[addDirectorToMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addDirectorToMovie]
@movieID int,
@directorName nvarchar(255),
@insertedID int OUTPUT
as
declare @gID int;
if not exists (select * from director where name = @directorName)
	begin
		insert into director(Name) values (@directorName);
		set @gID = SCOPE_IDENTITY();
	end
else
	begin
		select top 1 @gID = id from Director where Name = @directorName;
	end

insert into Director_Movie(DirectorID,MovieID) values(@gID, @movieID);
set @insertedID = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[addGenreToMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addGenreToMovie]
@movieID int,
@genreName nvarchar(255),
@insertedID int OUTPUT
as
declare @gID int;
if not exists (select id from Genre where Name = @genreName)
	begin
		insert into Genre(Name) values (@genreName);
		set @gID = SCOPE_IDENTITY();
	end
else
	begin
		select top 1 @gID = id from Genre where Name = @genreName;
	end

insert into Genre_Movie(GenreID,MovieID) values(@gID, @movieID);
set @insertedID = SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[addMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[addMovie]
@description text,
@lenght int,
@title nvarchar(255),
@posterUrl nvarchar(255),
@id int OUTPUT
as
insert into 
Movie(Description,Length, Title, PosterURl) values (@description, @lenght, @title, @posterURl)
set @id = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[addUser]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[authUser]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[authUser]
@username nvarchar(50),
@password nvarchar(255)
as
select Username, Password, isAdmin from AppUser where Username = @username and Password = @password;
GO
/****** Object:  StoredProcedure [dbo].[deleteMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[deleteMovie]
@id int
as
delete Genre_Movie where MovieID = @id;
delete Actor_Movie where MovieID = @id;
delete Director_Movie where MovieID = @id;
Delete Movie where ID = @ID;
GO
/****** Object:  StoredProcedure [dbo].[getAllMovies]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[getAllMovies]
as
Select m.ID, m.Description, m.Length, m.PosterURl, m.Title from Movie as m
GO
/****** Object:  StoredProcedure [dbo].[getMovieActors]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getMovieActors]
@id int
as
select a.ID, a.Name from Actor as a
inner join Actor_Movie as am
on am.ActorID = a.ID
where am.MovieID = @id
GO
/****** Object:  StoredProcedure [dbo].[getMovieByID]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[getMovieByID]
@id int
as
select * from Movie where ID = @id;
GO
/****** Object:  StoredProcedure [dbo].[getMovieDirectors]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getMovieDirectors]
@id int
as
select d.ID, d.Name from Director as d
inner join Director_Movie as dm
on dm.DirectorID = d.ID
where dm.MovieID = @id
GO
/****** Object:  StoredProcedure [dbo].[getMovieGenres]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getMovieGenres]
@id int
as
select g.ID, g.Name from Genre as g
inner join Genre_Movie as gm
on gm.GenreID = g.ID
where gm.MovieID = @id
GO
/****** Object:  StoredProcedure [dbo].[removeActorFromMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[removeActorFromMovie]
@movieID int,
@actorID int
as
delete from Actor_Movie	
where ActorID = @actorID and MovieID = @movieID;
GO
/****** Object:  StoredProcedure [dbo].[removeDirectorFromMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[removeDirectorFromMovie]
@movieID int,
@directorID int
as
delete from Director_Movie
where DirectorID = @directorID and MovieID = @movieID;
GO
/****** Object:  StoredProcedure [dbo].[removeGenreFromMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[removeGenreFromMovie]
@movieID int,
@genreID int
as
delete from Genre_Movie
where GenreID = @genreID and MovieID = @movieID;
GO
/****** Object:  StoredProcedure [dbo].[updateMovie]    Script Date: 6/18/2020 12:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[updateMovie]
@id int,
@description text,
@lenght int,
@title nvarchar(255)
as
update Movie set 
Description = @description, 
Length = @lenght, 
Title = @title
where ID = @id
GO
USE [master]
GO
ALTER DATABASE [cinestar_db] SET  READ_WRITE 
GO
INSERT INTO [dbo].[AppUser] (Username, Password, isAdmin) VALUES ('admin', 'admin', 1);
GO
